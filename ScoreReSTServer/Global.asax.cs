﻿using System.Web.Http;
using System.Web.Mvc;

using ScoreReSTServer.MessageHandlers;

namespace ScoreReSTServer
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.MessageHandlers.Add(new ApiKeyMessageHandler());
        }
    }
}
