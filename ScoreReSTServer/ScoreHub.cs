﻿using Microsoft.AspNet.SignalR;

namespace ScoreReSTServer
{
    public class ScoreHub : Hub
    {
        public void Send()
        {
            // Call broadcastMessage to update clients.
            Clients.All.broadcastMessage();
        }
    }
}