﻿using System.Collections.Generic;
using ScoreReSTServer.Models;

namespace ScoreReSTServer
{
    public interface IScorePersistence
    {
        long CreateScore(Score score);
        bool DeleteScore(long id);
        List<Score> RetrieveAllScores();
        Score RetrieveScore(long id);
        bool UpdateScore(long id, Score value);
    }
}