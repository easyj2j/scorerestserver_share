﻿using System.Collections.Generic;
using System.Configuration;

using MySql.Data.MySqlClient;
using ScoreReSTServer.Models;

namespace ScoreReSTServer
{
    #pragma warning disable 0168    // We catch MySqlException and cover the consequences.
    public class ScorePersistence : IScorePersistence
    {
        private const string TABLE_NAME = "timerscores";
        private const string COL_ID = "id";
        private const string COL_USERNAME = "username";
        private const string COL_LOCALID = "localid";
        private const string COL_LOCALNAME = "localname";
        private const string COL_REACTION = "reaction";
        private const string COL_MOTION = "motion";
        private const string COL_RESPONSE = "response";
        private const string COL_RETRIEVED = "retrieved";
        private const string COL_UPLOADED = "uploaded";
        private const string COL_DELETE_FLAG = "delete_flag";

        private const string COMMA = ", ";
        private const string TICK = "'";
        private const string TICK_COMMA_TICK = "','";
        private const string EQUAL_TICK = "='";

        private const string COLUMNS_LOCAL =
            COL_LOCALNAME + COMMA +
            COL_REACTION  + COMMA +
            COL_MOTION    + COMMA +
            COL_RESPONSE  + COMMA +
            COL_RETRIEVED;

        private const string SELECT_ID    = "SELECT " + COL_ID + " FROM " + TABLE_NAME;
        private const string SELECT_LOCAL = "SELECT " + COL_ID + COMMA + COLUMNS_LOCAL + " FROM " + TABLE_NAME;
        private const string SELECT_ALL   = "SELECT * FROM " + TABLE_NAME;

        private const string WHERE_ID_EQ = " WHERE id = ";

        private const string COLUMNS_TO_INSERT = " (" + COL_LOCALID + COMMA + COLUMNS_LOCAL + ")";
        private const string DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

        private string connectionString;

        public ScorePersistence()
        {
            // [App can be running locally but accessing azure DB.]
            // When published, Azure will replace localDb connection string w one pointing to DB on Azure.
            connectionString = ConfigurationManager.ConnectionStrings["localDb"].ConnectionString;
        }

        // ExecuteNonQuery returns numberOfRows only for INSERT, UPDATE, and DELETE; otherwise -1
        public long CreateScore(Score score)
        {
            long id = -1;

            MySqlConnection dbConnection = new MySqlConnection();
            try
            {
                dbConnection.ConnectionString = connectionString;
                dbConnection.Open();

                string sqlString = "INSERT INTO " + TABLE_NAME + COLUMNS_TO_INSERT + " VALUES ('" +
                    score.LocalID   + TICK_COMMA_TICK + 
                    score.LocalName + TICK_COMMA_TICK + 
                    score.Reaction  + TICK_COMMA_TICK + 
                    score.Motion    + TICK_COMMA_TICK + 
                    score.Response  + TICK_COMMA_TICK + 
                    score.Retrieved.ToString(DATETIME_FORMAT) + "')";

                MySqlCommand command = new MySqlCommand(sqlString, dbConnection);
                command.ExecuteNonQuery();
                id = command.LastInsertedId;
            }
            catch (MySqlException ex) {} // TODO: log exception
            finally { dbConnection.Close(); }

            return id;
        }

        // <param name="id">id of the row to return. If id = 0, the latest score is returned.</param>
        public Score RetrieveScore(long id)
        {
            Score score = null;

            MySqlConnection dbConnection = new MySqlConnection();
            try
            {
                dbConnection.ConnectionString = connectionString;
                dbConnection.Open();

                string sqlString;
                if(id == 0)
                {
                    sqlString = SELECT_ALL + " ORDER BY id DESC " + " LIMIT 1";
                }
                else
                {
                    sqlString = SELECT_ALL + WHERE_ID_EQ + id.ToString();
                }

                MySqlCommand command = new MySqlCommand(sqlString, dbConnection);

                MySqlDataReader reader = command.ExecuteReader();
                if(reader.Read())
                {
                    score = CreateScoreFromReader(reader);
                }
            }
            catch(MySqlException ex) {} // log exception
            finally { dbConnection.Close(); }

            return score;
        }

        public List<Score> RetrieveAllScores()
        {
            List<Score> scores = null;

            MySqlConnection dbConnection = new MySqlConnection();
            try
            {
                dbConnection.ConnectionString = connectionString;
                dbConnection.Open();

                scores = new List<Score>();

                string sqlString = SELECT_ALL + " ORDER BY id DESC";

                MySqlCommand command = new MySqlCommand(sqlString, dbConnection);
                MySqlDataReader reader = command.ExecuteReader();       // TODO: close reader in finally?
                while(reader.Read())
                {
                    scores.Add(CreateScoreFromReader(reader));
                }
            }
            catch(MySqlException ex) {} // log exception
            finally { dbConnection.Close(); }

            return scores;
        }

        // TODO: allow updating score just passing fields needing updating
        public bool UpdateScore(long id, Score value)
        {
            bool success = false;

            MySqlConnection dbConnection = new MySqlConnection();
            try
            {
                dbConnection.ConnectionString = connectionString;
                dbConnection.Open();

                string sqlString = SELECT_ID + WHERE_ID_EQ + id.ToString();

                MySqlCommand command = new MySqlCommand(sqlString, dbConnection);
                MySqlDataReader reader = command.ExecuteReader();
                if(reader.Read())
                {
                    // id found
                    reader.Close();
                    sqlString = "UPDATE " + TABLE_NAME + " SET " +
                        COL_LOCALID   + EQUAL_TICK + value.LocalID   + TICK +
                        COL_LOCALNAME + EQUAL_TICK + value.LocalName + TICK +
                        COL_REACTION  + EQUAL_TICK + value.Reaction  + TICK +
                        COL_MOTION    + EQUAL_TICK + value.Motion    + TICK +
                        COL_RESPONSE  + EQUAL_TICK + value.Response  + TICK +
                        COL_RETRIEVED + EQUAL_TICK + value.Retrieved.ToString(DATETIME_FORMAT) + TICK +
                        WHERE_ID_EQ  + id.ToString(); 
                    command = new MySqlCommand(sqlString, dbConnection);
                    command.ExecuteNonQuery();
                    success = true;
                }
            }
            catch (MySqlException ex) {} // log exception
            finally { dbConnection.Close(); }

            return success;
        }

        public bool DeleteScore(long id)
        {
            bool success = false;

            MySqlConnection dbConnection = new MySqlConnection();
            try
            {
                dbConnection.ConnectionString = connectionString;
                dbConnection.Open();

                // Check if table contains id
                string sqlString = SELECT_ID + WHERE_ID_EQ + id.ToString();
                object obj = (new MySqlCommand(sqlString, dbConnection)).ExecuteScalar();
                if(obj != null)
                {
                    sqlString = "DELETE FROM " + TABLE_NAME + WHERE_ID_EQ + id.ToString();
                    long numberOfRows = (new MySqlCommand(sqlString, dbConnection)).ExecuteNonQuery();
                    success = (numberOfRows == 1) ? true : false;
                }
            }
            catch(MySqlException ex) {} // log exception
            finally { dbConnection.Close(); }

            return success;
        }

        // Integer is column #
        // Current ReST api only returns those values local to client.
        // Keep, but comment out, UserName, Uploaded and DeleteFlag in case
        // want to return them later.
        // See Score class.
        private Score CreateScoreFromReader(MySqlDataReader reader)
        {
            Score score = new Score();
            score.ID = reader.GetInt32(COL_ID);
            //score.UserName = reader.GetString(COL_USERNAME);
            score.LocalID = reader.GetInt32(COL_LOCALID);
            score.LocalName = reader.GetString(COL_LOCALNAME);
            score.Reaction = reader.GetInt32(COL_REACTION);
            score.Motion = reader.GetInt32(COL_MOTION);
            score.Response = reader.GetInt32(COL_RESPONSE);
            score.Retrieved = reader.GetDateTime(COL_RETRIEVED);
            //score.Uploaded = reader.GetDateTime(COL_UPLOADED);
            //score.DeleteFlag = reader.GetBoolean(COL_DELETE_FLAG);
            return score;
        }
    }
    #pragma warning restore 0168
}